EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text GLabel 1400 3500 0    50   Input ~ 0
1.45
Text GLabel 1400 3400 0    50   Input ~ 0
1.43
Text GLabel 1400 3800 0    50   Input ~ 0
1.51
Text GLabel 1400 3600 0    50   Input ~ 0
1.47
Text GLabel 1400 3700 0    50   Input ~ 0
1.49
Text GLabel 1900 3800 2    50   Input ~ 0
1.52
Text GLabel 1900 3600 2    50   Input ~ 0
1.48
Text GLabel 1900 3500 2    50   Input ~ 0
1.46
Text GLabel 1900 3400 2    50   Input ~ 0
1.44
Text GLabel 2800 3300 0    50   Input ~ 0
1.41
Text GLabel 1900 3300 2    50   Input ~ 0
1.42
Text GLabel 2800 3200 0    50   Input ~ 0
1.39
Text GLabel 1400 3200 0    50   Input ~ 0
1.39
Text GLabel 1400 3300 0    50   Input ~ 0
1.41
Text GLabel 1900 3200 2    50   Input ~ 0
1.40
Text GLabel 2800 3100 0    50   Input ~ 0
1.37
Text GLabel 2800 3000 0    50   Input ~ 0
1.35
Text GLabel 2800 2900 0    50   Input ~ 0
1.33
Text GLabel 2800 2800 0    50   Input ~ 0
1.31
Text GLabel 2800 2700 0    50   Input ~ 0
1.29
Text GLabel 1400 3100 0    50   Input ~ 0
1.37
Text GLabel 1400 2900 0    50   Input ~ 0
1.33
Text GLabel 1400 3000 0    50   Input ~ 0
1.35
Text GLabel 1900 3100 2    50   Input ~ 0
1.38
Text GLabel 1900 2900 2    50   Input ~ 0
1.34
Text GLabel 1900 3000 2    50   Input ~ 0
1.36
Text GLabel 1400 2400 0    50   Input ~ 0
1.23
Text GLabel 1400 2500 0    50   Input ~ 0
1.25
Text GLabel 1400 2300 0    50   Input ~ 0
1.21
Text GLabel 1400 2200 0    50   Input ~ 0
1.19
Text GLabel 1900 2800 2    50   Input ~ 0
1.32
Text GLabel 1400 2600 0    50   Input ~ 0
1.27
Text GLabel 1400 2700 0    50   Input ~ 0
1.29
Text GLabel 1400 2800 0    50   Input ~ 0
1.31
$Comp
L MLAB_HEADER:HEADER_2x26 J1
U 1 1 5D194D38
P 1650 2550
F 0 "J1" H 1650 4153 60  0000 C CNN
F 1 "HEADER_2x26" H 1650 4047 60  0000 C CNN
F 2 "Spacemanic_header:Header_2x26_2.54mm" H 1650 3941 60  0001 C CNN
F 3 "" H 1650 3800 60  0000 C CNN
	1    1650 2550
	1    0    0    -1  
$EndComp
Text GLabel 1400 1900 0    50   Input ~ 0
1.13
Text GLabel 1400 2000 0    50   Input ~ 0
1.15
Text GLabel 1400 2100 0    50   Input ~ 0
1.17
Text GLabel 1400 1800 0    50   Input ~ 0
1.11
Text GLabel 1400 1700 0    50   Input ~ 0
1.9
Text GLabel 1400 1300 0    50   Input ~ 0
1.1
Text GLabel 1400 1600 0    50   Input ~ 0
1.7
Text GLabel 1400 1500 0    50   Input ~ 0
1.5
Text GLabel 1400 1400 0    50   Input ~ 0
1.3
Text GLabel 1900 2000 2    50   Input ~ 0
1.16
Text GLabel 1900 1900 2    50   Input ~ 0
1.14
Text GLabel 1900 1800 2    50   Input ~ 0
1.12
Text GLabel 1900 1700 2    50   Input ~ 0
1.10
Text GLabel 1900 2100 2    50   Input ~ 0
1.18
Text GLabel 1900 2200 2    50   Input ~ 0
1.20
Text GLabel 1900 2600 2    50   Input ~ 0
1.28
Text GLabel 1900 2700 2    50   Input ~ 0
1.30
Text GLabel 1900 2400 2    50   Input ~ 0
1.24
Text GLabel 1900 2300 2    50   Input ~ 0
1.22
Text GLabel 1900 2500 2    50   Input ~ 0
1.26
Text GLabel 1900 1500 2    50   Input ~ 0
1.6
Text GLabel 1900 1300 2    50   Input ~ 0
1.2
Text GLabel 1900 1600 2    50   Input ~ 0
1.8
Text GLabel 1900 1400 2    50   Input ~ 0
1.4
Text GLabel 3300 1400 2    50   Input ~ 0
1.4
Text GLabel 3300 1600 2    50   Input ~ 0
1.8
Text GLabel 3300 1500 2    50   Input ~ 0
1.6
Text GLabel 3300 1700 2    50   Input ~ 0
1.10
Text GLabel 3300 1300 2    50   Input ~ 0
1.2
Text GLabel 2800 1700 0    50   Input ~ 0
1.9
Text GLabel 2800 1400 0    50   Input ~ 0
1.3
Text GLabel 2800 1300 0    50   Input ~ 0
1.1
Text GLabel 2800 1500 0    50   Input ~ 0
1.5
Text GLabel 2800 1600 0    50   Input ~ 0
1.7
Text GLabel 3300 2400 2    50   Input ~ 0
1.24
Text GLabel 3300 2500 2    50   Input ~ 0
1.26
Text GLabel 3300 2300 2    50   Input ~ 0
1.22
Text GLabel 3300 2700 2    50   Input ~ 0
1.30
Text GLabel 3300 2600 2    50   Input ~ 0
1.28
Text GLabel 2800 2600 0    50   Input ~ 0
1.27
Text GLabel 2800 2400 0    50   Input ~ 0
1.23
Text GLabel 2800 2500 0    50   Input ~ 0
1.25
Text GLabel 2800 2200 0    50   Input ~ 0
1.19
Text GLabel 2800 2300 0    50   Input ~ 0
1.21
Text GLabel 3300 2200 2    50   Input ~ 0
1.20
Text GLabel 3300 1900 2    50   Input ~ 0
1.14
Text GLabel 3300 2000 2    50   Input ~ 0
1.16
Text GLabel 3300 2100 2    50   Input ~ 0
1.18
Text GLabel 3300 1800 2    50   Input ~ 0
1.12
Text GLabel 2800 2100 0    50   Input ~ 0
1.17
Text GLabel 2800 2000 0    50   Input ~ 0
1.15
Text GLabel 2800 1800 0    50   Input ~ 0
1.11
Text GLabel 2800 1900 0    50   Input ~ 0
1.13
Text GLabel 3300 2800 2    50   Input ~ 0
1.32
Text GLabel 3300 3100 2    50   Input ~ 0
1.38
Text GLabel 3300 3000 2    50   Input ~ 0
1.36
Text GLabel 3300 2900 2    50   Input ~ 0
1.34
Text GLabel 3300 3300 2    50   Input ~ 0
1.42
Text GLabel 3300 3200 2    50   Input ~ 0
1.40
$Comp
L MLAB_HEADER:HEADER_2x26 J3
U 1 1 5E34AA3A
P 3050 2550
F 0 "J3" H 3050 4153 60  0000 C CNN
F 1 "HEADER_2x26" H 3050 4047 60  0000 C CNN
F 2 "Spacemanic_header:Header_2x26_2.54mm" H 3050 3941 60  0001 C CNN
F 3 "" H 3050 3800 60  0000 C CNN
	1    3050 2550
	1    0    0    -1  
$EndComp
Text GLabel 2800 5950 0    50   Input ~ 0
2.33
Text GLabel 2800 6150 0    50   Input ~ 0
2.37
Text GLabel 2800 6050 0    50   Input ~ 0
2.35
Text GLabel 3300 6150 2    50   Input ~ 0
2.38
Text GLabel 2800 6750 0    50   Input ~ 0
2.49
Text GLabel 2800 6850 0    50   Input ~ 0
2.51
Text GLabel 3300 6850 2    50   Input ~ 0
2.52
Text GLabel 3300 6750 2    50   Input ~ 0
2.50
Text GLabel 3300 6650 2    50   Input ~ 0
2.48
Text GLabel 3300 6550 2    50   Input ~ 0
2.46
Text GLabel 3300 6350 2    50   Input ~ 0
2.42
Text GLabel 3300 6250 2    50   Input ~ 0
2.40
Text GLabel 3300 6450 2    50   Input ~ 0
2.44
Text GLabel 1900 6050 2    50   Input ~ 0
2.36
Text GLabel 1900 5950 2    50   Input ~ 0
2.34
Text GLabel 1900 5850 2    50   Input ~ 0
2.32
Text GLabel 1900 5750 2    50   Input ~ 0
2.30
Text GLabel 1900 5650 2    50   Input ~ 0
2.28
Text GLabel 3300 6050 2    50   Input ~ 0
2.36
Text GLabel 3300 5650 2    50   Input ~ 0
2.28
Text GLabel 3300 5750 2    50   Input ~ 0
2.30
Text GLabel 3300 5950 2    50   Input ~ 0
2.34
Text GLabel 3300 5850 2    50   Input ~ 0
2.32
Text GLabel 2800 6250 0    50   Input ~ 0
2.39
Text GLabel 2800 6650 0    50   Input ~ 0
2.47
Text GLabel 2800 6550 0    50   Input ~ 0
2.45
Text GLabel 2800 6450 0    50   Input ~ 0
2.43
Text GLabel 2800 6350 0    50   Input ~ 0
2.41
Text GLabel 1900 6150 2    50   Input ~ 0
2.38
Text GLabel 1900 6250 2    50   Input ~ 0
2.40
Text GLabel 1900 6350 2    50   Input ~ 0
2.42
Text GLabel 1900 6450 2    50   Input ~ 0
2.44
Text GLabel 1900 6550 2    50   Input ~ 0
2.46
Text GLabel 1900 6650 2    50   Input ~ 0
2.48
Text GLabel 1900 6750 2    50   Input ~ 0
2.50
Text GLabel 1400 6850 0    50   Input ~ 0
2.51
Text GLabel 1900 6850 2    50   Input ~ 0
2.52
Text GLabel 1400 6750 0    50   Input ~ 0
2.49
Text GLabel 1400 6450 0    50   Input ~ 0
2.43
Text GLabel 1400 6650 0    50   Input ~ 0
2.47
Text GLabel 1400 6350 0    50   Input ~ 0
2.41
Text GLabel 1400 6550 0    50   Input ~ 0
2.45
Text GLabel 1400 6250 0    50   Input ~ 0
2.39
Text GLabel 1400 5850 0    50   Input ~ 0
2.31
Text GLabel 1400 5950 0    50   Input ~ 0
2.33
Text GLabel 1400 6150 0    50   Input ~ 0
2.37
Text GLabel 1400 6050 0    50   Input ~ 0
2.35
Text GLabel 1400 5750 0    50   Input ~ 0
2.29
Text GLabel 1400 5650 0    50   Input ~ 0
2.27
Text GLabel 1400 5450 0    50   Input ~ 0
2.23
Text GLabel 1400 5350 0    50   Input ~ 0
2.21
Text GLabel 1400 5550 0    50   Input ~ 0
2.25
$Comp
L MLAB_HEADER:HEADER_2x26 J2
U 1 1 5D197155
P 1650 5600
F 0 "J2" H 1650 7203 60  0000 C CNN
F 1 "HEADER_2x26" H 1650 7097 60  0000 C CNN
F 2 "Spacemanic_header:Header_2x26_2.54mm" H 1650 6991 60  0001 C CNN
F 3 "" H 1650 6850 60  0000 C CNN
	1    1650 5600
	1    0    0    -1  
$EndComp
Text GLabel 1400 5250 0    50   Input ~ 0
2.19
Text GLabel 1400 5150 0    50   Input ~ 0
2.17
Text GLabel 1400 5050 0    50   Input ~ 0
2.15
Text GLabel 1400 4950 0    50   Input ~ 0
2.13
Text GLabel 1400 4850 0    50   Input ~ 0
2.11
Text GLabel 1400 4450 0    50   Input ~ 0
2.3
Text GLabel 1400 4550 0    50   Input ~ 0
2.5
Text GLabel 1400 4350 0    50   Input ~ 0
2.1
Text GLabel 1400 4750 0    50   Input ~ 0
2.9
Text GLabel 1400 4650 0    50   Input ~ 0
2.7
Text GLabel 1900 5250 2    50   Input ~ 0
2.20
Text GLabel 1900 5350 2    50   Input ~ 0
2.22
Text GLabel 1900 5450 2    50   Input ~ 0
2.24
Text GLabel 1900 5550 2    50   Input ~ 0
2.26
Text GLabel 1900 5050 2    50   Input ~ 0
2.16
Text GLabel 1900 5150 2    50   Input ~ 0
2.18
Text GLabel 1900 4750 2    50   Input ~ 0
2.10
Text GLabel 1900 4850 2    50   Input ~ 0
2.12
Text GLabel 1900 4950 2    50   Input ~ 0
2.14
Text GLabel 2800 5750 0    50   Input ~ 0
2.29
Text GLabel 2800 5850 0    50   Input ~ 0
2.31
Text GLabel 2800 5650 0    50   Input ~ 0
2.27
Text GLabel 2800 5450 0    50   Input ~ 0
2.23
Text GLabel 2800 5550 0    50   Input ~ 0
2.25
$Comp
L MLAB_HEADER:HEADER_2x26 J4
U 1 1 5E34ACC1
P 3050 5600
F 0 "J4" H 3050 7203 60  0000 C CNN
F 1 "HEADER_2x26" H 3050 7097 60  0000 C CNN
F 2 "Spacemanic_header:Header_2x26_2.54mm" H 3050 6991 60  0001 C CNN
F 3 "" H 3050 6850 60  0000 C CNN
	1    3050 5600
	1    0    0    -1  
$EndComp
Text GLabel 2800 4650 0    50   Input ~ 0
2.7
Text GLabel 2800 4550 0    50   Input ~ 0
2.5
Text GLabel 2800 4950 0    50   Input ~ 0
2.13
Text GLabel 2800 4850 0    50   Input ~ 0
2.11
Text GLabel 2800 4750 0    50   Input ~ 0
2.9
Text GLabel 2800 5150 0    50   Input ~ 0
2.17
Text GLabel 2800 5250 0    50   Input ~ 0
2.19
Text GLabel 2800 5350 0    50   Input ~ 0
2.21
Text GLabel 2800 5050 0    50   Input ~ 0
2.15
Text GLabel 3300 4950 2    50   Input ~ 0
2.14
Text GLabel 3300 4750 2    50   Input ~ 0
2.10
Text GLabel 3300 4850 2    50   Input ~ 0
2.12
Text GLabel 3300 5050 2    50   Input ~ 0
2.16
Text GLabel 3300 5150 2    50   Input ~ 0
2.18
Text GLabel 3300 5550 2    50   Input ~ 0
2.26
Text GLabel 3300 5350 2    50   Input ~ 0
2.22
Text GLabel 3300 5450 2    50   Input ~ 0
2.24
Text GLabel 3300 5250 2    50   Input ~ 0
2.20
Text GLabel 1900 4650 2    50   Input ~ 0
2.8
Text GLabel 1900 4550 2    50   Input ~ 0
2.6
Text GLabel 1900 4350 2    50   Input ~ 0
2.2
Text GLabel 1900 4450 2    50   Input ~ 0
2.4
Text GLabel 1900 3700 2    50   Input ~ 0
1.50
Text GLabel 2800 4450 0    50   Input ~ 0
2.3
Text GLabel 2800 4350 0    50   Input ~ 0
2.1
Text GLabel 2800 3700 0    50   Input ~ 0
1.49
Text GLabel 2800 3800 0    50   Input ~ 0
1.51
Text GLabel 2800 3600 0    50   Input ~ 0
1.47
Text GLabel 3300 3800 2    50   Input ~ 0
1.52
Text GLabel 3300 3700 2    50   Input ~ 0
1.50
Text GLabel 3300 3600 2    50   Input ~ 0
1.48
Text GLabel 2800 3500 0    50   Input ~ 0
1.45
Text GLabel 2800 3400 0    50   Input ~ 0
1.43
Text GLabel 3300 3500 2    50   Input ~ 0
1.46
Text GLabel 3300 3400 2    50   Input ~ 0
1.44
Text GLabel 3300 4550 2    50   Input ~ 0
2.6
Text GLabel 3300 4650 2    50   Input ~ 0
2.8
Text GLabel 3300 4350 2    50   Input ~ 0
2.2
Text GLabel 3300 4450 2    50   Input ~ 0
2.4
$Comp
L MLAB_MECHANICAL:HOLE M3
U 1 1 5D096D66
P 9450 2075
F 0 "M3" V 9325 2075 60  0000 C CNN
F 1 "HOLE" V 9350 2075 60  0001 C CNN
F 2 "Mounting_Holes:MountingHole_3.2mm_M3_DIN965_Pad" H 9469 2183 60  0001 C CNN
F 3 "" H 9450 2075 60  0000 C CNN
	1    9450 2075
	0    1    1    0   
$EndComp
$Comp
L MLAB_MECHANICAL:HOLE M4
U 1 1 5D097003
P 9675 2075
F 0 "M4" V 9550 2075 60  0000 C CNN
F 1 "HOLE" V 9575 2100 60  0001 C CNN
F 2 "Mounting_Holes:MountingHole_3.2mm_M3_DIN965_Pad" H 9694 2183 60  0001 C CNN
F 3 "" H 9675 2075 60  0000 C CNN
	1    9675 2075
	0    1    1    0   
$EndComp
$Comp
L MLAB_MECHANICAL:HOLE M2
U 1 1 5D096AD3
P 9200 2075
F 0 "M2" V 9075 2075 60  0000 C CNN
F 1 "HOLE" V 9100 2075 60  0001 C CNN
F 2 "Mounting_Holes:MountingHole_3.2mm_M3_DIN965_Pad" H 9219 2183 60  0001 C CNN
F 3 "" H 9200 2075 60  0000 C CNN
	1    9200 2075
	0    1    1    0   
$EndComp
$Comp
L MLAB_MECHANICAL:HOLE M1
U 1 1 5D096865
P 8950 2075
F 0 "M1" V 8825 2075 60  0000 C CNN
F 1 "HOLE" V 8850 2075 60  0001 C CNN
F 2 "Mounting_Holes:MountingHole_3.2mm_M3_DIN965_Pad" H 8969 2183 60  0001 C CNN
F 3 "" H 8950 2075 60  0000 C CNN
	1    8950 2075
	0    1    1    0   
$EndComp
$Comp
L MLAB_HEADER:HEADER_2x13_PARALLEL J8
U 1 1 5E925E92
P 7600 2950
F 0 "J8" H 7728 3061 60  0000 L CNN
F 1 "HEADER_2x15_PARALLEL" H 7728 2955 60  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x13_P2.54mm_Vertical" H 7728 2849 60  0001 L CNN
F 3 "" H 7600 3650 60  0000 C CNN
	1    7600 2950
	1    0    0    -1  
$EndComp
$Comp
L MLAB_HEADER:HEADER_1x13 J7
U 1 1 5E92C3F6
P 7000 2950
F 0 "J7" H 6950 3850 60  0000 C CNN
F 1 "HEADER_1x15" H 6925 3725 60  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x13_P2.54mm_Vertical" H 6917 3791 60  0001 C CNN
F 3 "" H 7000 3650 60  0000 C CNN
	1    7000 2950
	-1   0    0    -1  
$EndComp
$Comp
L MLAB_HEADER:HEADER_1x13 J5
U 1 1 5E929BD7
P 6300 2950
F 0 "J5" H 6075 3825 60  0000 C CNN
F 1 "HEADER_2x15_PARALLEL" H 6100 3725 60  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x13_P2.54mm_Vertical" H 5475 1675 60  0001 C CNN
F 3 "" H 6300 3650 60  0000 C CNN
	1    6300 2950
	1    0    0    -1  
$EndComp
$Comp
L MLAB_HEADER:HEADER_2x13_PARALLEL J6
U 1 1 5E922E1F
P 5725 2950
F 0 "J6" H 5853 3061 60  0000 L CNN
F 1 "HEADER_2x15_PARALLEL" H 5853 2955 60  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x13_P2.54mm_Vertical" H 5853 2849 60  0001 L CNN
F 3 "" H 5725 3650 60  0000 C CNN
	1    5725 2950
	-1   0    0    -1  
$EndComp
Wire Wire Line
	6100 2350 5975 2350
Wire Wire Line
	5975 2450 6100 2450
Wire Wire Line
	6100 2550 5975 2550
Wire Wire Line
	5975 2650 6100 2650
Wire Wire Line
	6100 2750 5975 2750
Wire Wire Line
	5975 2850 6100 2850
Wire Wire Line
	6100 2950 5975 2950
Wire Wire Line
	5975 3050 6100 3050
Wire Wire Line
	6100 3150 5975 3150
Wire Wire Line
	5975 3250 6100 3250
Wire Wire Line
	6100 3350 5975 3350
Wire Wire Line
	5975 3450 6100 3450
Wire Wire Line
	6100 3550 5975 3550
Wire Wire Line
	7200 2350 7350 2350
Wire Wire Line
	7350 2450 7200 2450
Wire Wire Line
	7200 2550 7350 2550
Wire Wire Line
	7350 2650 7200 2650
Wire Wire Line
	7200 2750 7350 2750
Wire Wire Line
	7350 2850 7200 2850
Wire Wire Line
	7200 2950 7350 2950
Wire Wire Line
	7350 3050 7200 3050
Wire Wire Line
	7200 3150 7350 3150
Wire Wire Line
	7350 3250 7200 3250
Wire Wire Line
	7200 3350 7350 3350
Wire Wire Line
	7350 3450 7200 3450
Wire Wire Line
	7200 3550 7350 3550
$Comp
L MLAB_HEADER:HEADER_2x13_PARALLEL J12
U 1 1 5EB8373D
P 7600 4700
F 0 "J12" H 7728 4811 60  0000 L CNN
F 1 "HEADER_2x15_PARALLEL" H 7728 4705 60  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x13_P2.54mm_Vertical" H 7728 4599 60  0001 L CNN
F 3 "" H 7600 5400 60  0000 C CNN
	1    7600 4700
	1    0    0    -1  
$EndComp
$Comp
L MLAB_HEADER:HEADER_1x13 J11
U 1 1 5EB83743
P 7000 4700
F 0 "J11" H 6950 5600 60  0000 C CNN
F 1 "HEADER_1x15" H 6925 5475 60  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x13_P2.54mm_Vertical" H 6917 5541 60  0001 C CNN
F 3 "" H 7000 5400 60  0000 C CNN
	1    7000 4700
	-1   0    0    -1  
$EndComp
$Comp
L MLAB_HEADER:HEADER_1x13 J10
U 1 1 5EB83749
P 6300 4700
F 0 "J10" H 6075 5575 60  0000 C CNN
F 1 "HEADER_2x15_PARALLEL" H 6100 5475 60  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x13_P2.54mm_Vertical" H 5475 3425 60  0001 C CNN
F 3 "" H 6300 5400 60  0000 C CNN
	1    6300 4700
	1    0    0    -1  
$EndComp
$Comp
L MLAB_HEADER:HEADER_2x13_PARALLEL J9
U 1 1 5EB8374F
P 5725 4700
F 0 "J9" H 5853 4811 60  0000 L CNN
F 1 "HEADER_2x15_PARALLEL" H 5853 4705 60  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x13_P2.54mm_Vertical" H 5853 4599 60  0001 L CNN
F 3 "" H 5725 5400 60  0000 C CNN
	1    5725 4700
	-1   0    0    -1  
$EndComp
Wire Wire Line
	6100 4100 5975 4100
Wire Wire Line
	5975 4200 6100 4200
Wire Wire Line
	6100 4300 5975 4300
Wire Wire Line
	5975 4400 6100 4400
Wire Wire Line
	6100 4500 5975 4500
Wire Wire Line
	5975 4600 6100 4600
Wire Wire Line
	6100 4700 5975 4700
Wire Wire Line
	5975 4800 6100 4800
Wire Wire Line
	6100 4900 5975 4900
Wire Wire Line
	5975 5000 6100 5000
Wire Wire Line
	6100 5100 5975 5100
Wire Wire Line
	5975 5200 6100 5200
Wire Wire Line
	6100 5300 5975 5300
Wire Wire Line
	7200 4100 7350 4100
Wire Wire Line
	7350 4200 7200 4200
Wire Wire Line
	7200 4300 7350 4300
Wire Wire Line
	7350 4400 7200 4400
Wire Wire Line
	7200 4500 7350 4500
Wire Wire Line
	7350 4600 7200 4600
Wire Wire Line
	7200 4700 7350 4700
Wire Wire Line
	7350 4800 7200 4800
Wire Wire Line
	7200 4900 7350 4900
Wire Wire Line
	7350 5000 7200 5000
Wire Wire Line
	7200 5100 7350 5100
Wire Wire Line
	7350 5200 7200 5200
Wire Wire Line
	7200 5300 7350 5300
$EndSCHEMATC
